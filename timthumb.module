<?php
/**
 * @file
 * Main file for TimThumb Formatter module.
 */

 /**
 * Implements hook_theme().
 */
function timthumb_theme() {
  return array(
    'timthumb_formatter' => array(
      'variables' => array('item' => NULL, 'settings' => NULL),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function timthumb_field_formatter_info() {
  $formatters['timthumb'] = array(
    'label' => t('TimThumb'),
    'field types' => array('image'),
    'settings' => array(
			'dimensions' => array(
      'width' => NULL,
      'height' => NULL,
			),
      'quality' => NULL,
      'alignment' => '',
      'zoom_crop' => '',
			'filters' => NULL,
      'sharpen' => false,
      'canvas_colour' => NULL,
			'canvas_transparency' => false,
    ),
  );

	return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function timthumb_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element['dimensions'] = array(
    '#type' => 'item',
    '#title' => t('Image dimensions'),
    '#element_validate' => array('_timthumb_field_dimensions_validate'),    
    '#field_prefix' => '<div class="container-inline">',
    '#field_suffix' => '</div>',
    '#description' => t('The image size expressed as WIDTHxHEIGHT (e.g. 640x480).'),
  );
  $element['dimensions']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('width'),
    '#title_display' => 'invisible',
    '#default_value' => $settings['dimensions']['width'],
    '#size' => 5,
    '#maxlength' => 5,
    '#field_suffix' => ' x ',
  );
  $element['dimensions']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('height'),
    '#title_display' => 'invisible',
    '#default_value' => $settings['dimensions']['height'],
    '#size' => 5,
    '#maxlength' => 5,
    '#field_suffix' => ' ' . t('pixels'),
  ); 
  $element['quality'] = array(
    '#type' => 'number',
    '#title' => t('Compression quality'),
		'#min' => 0,
		'#max' => 100,
    '#default_value' => $settings['quality'],    
  );
  $element['alignment'] = array(
    '#type' => 'select',
    '#title' => t('Crop alignment'),
    '#default_value' => $settings['alignment'],
		'#options' => array(
			'' => 'Default',
      'c' => 'center',
      't' => 'top',
      'b' => 'bottom',
			'r' => 'right',
			'l' => 'left',
      'tl' => 'top left',
      'tr' => 'top right',
			'bl' => 'bottom left',
			'br' => 'bottom right',
    ),
  );
  $element['zoom_crop'] = array(
    '#type' => 'select',
    '#title' => t('Cropping and scaling'),
		'#description' => t('For more information see: <a href="@filters">TimThumb crop scaling tutorial</a>.' , array('@filters' => 'http://www.binarymoon.co.uk/2011/03/timthumb-proportional-scaling-security-improvements/')),
    '#default_value' => $settings['zoom_crop'],
		'#options' => array(
			'' => 'Default',
      '0' => '0',
      '1' => '1',
      '2' => '2',
			'3' => '3',
    ),
  );
	$element['filters'] = array(
    '#type' => 'textfield',
    '#title' => t('Image filters'),
    '#description' => t('For more information see: <a href="@filters">TimThumb image filter tutorial</a>.' , array('@filters' => 'http://www.binarymoon.co.uk/2010/08/timthumb-image-filters/')),
    '#default_value' => $settings['filters'],
  );
  $element['sharpen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sharpen filter'),
    '#default_value' => $settings['sharpen']
  );
	$element['canvas_colour'] = array(
    '#type' => 'color',
    '#title' => t('Background colour'),
    '#default_value' => $settings['canvas_colour'],
  );
  $element['canvas_transparency'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transparency'),
    '#default_value' => $settings['canvas_transparency'],
  );

  return $element;
}

/**
 * Element validate function for dimensions fields.
 */
function _timthumb_field_dimensions_validate($element, &$form_state) {
  if (!empty($element['width']['#value']) || !empty($element['height']['#value'])) {
    foreach (array('width', 'height') as $dimension) {
      $value = $element[$dimension]['#value'];
      if (!is_numeric($value)) {
        form_error($element[$dimension], t('Width and height values must be numeric.'));
        return;
      }
      if (intval($value) == 0) {
        form_error($element[$dimension], t('Both a width and height value must be specified in the !name field.', array('!name' => $element['#title'])));
        return;
      }
    }
	}
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function timthumb_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  if (!empty($settings['dimensions']['width']) || !empty($settings['dimensions']['height'])) {
    $summary[] = t('Image dimensions: @width x @height', array('@width' => $settings['dimensions']['width'], '@height' => $settings['dimensions']['height']));
  }else{
		$summary[] = t('Default dimensions : 100 x 100');
	}
  if (!empty($settings['quality'])) {
    $summary[] = t('quality: @quality', array('@quality' => $settings['quality']));
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function timthumb_field_formatter_view(EntityInterface $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

	foreach ($items as $delta => $item) {
    $element[$delta] = array(
      '#theme' => 'timthumb_formatter',
      '#item' => $item,
			'#settings' => $settings,
    );
  }
  return $element;
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - settings: An array containing the timthumb formatter setting data.
 *
 * @ingroup themeable
 */
function theme_timthumb_formatter($variables) {
	$item = $variables['item'];
	$settings = $variables['settings'];

	$url = '';
	if(!empty($settings['dimensions']['width'])) {
		$url = '&amp;w='.$settings['dimensions']['width'];
	}
	if(!empty($settings['dimensions']['height'])) {
		$url .= '&amp;h='.$settings['dimensions']['height'];
	}
	if(!empty($settings['quality'])) {
		$url .= '&amp;q='.$settings['quality'];
	}
	if(!empty($settings['alignment'])) {
		$url .= '&amp;a='.$settings['alignment'];
	}
	if(!empty($settings['zoom_crop'])) {
		$url .= '&amp;zc='.$settings['zoom_crop'];
	}
	if(!empty($settings['filters'])) {
		$url .= '&amp;f='.$settings['filters'];
	}
	if(!empty($settings['sharpen'])) {
		$url .= '&amp;s='.$settings['sharpen'];
	}
	if(!empty($settings['canvas_colour'])) {
		$url .= '&amp;cc='.$settings['canvas_colour'];
	}
	if(!empty($settings['canvas_transparency'])) {
		$url .= '&amp;ct='.$settings['canvas_transparency'];
	}

  $output = '<img src="'.base_path().drupal_get_path('module', 'timthumb').'/timthumb.php?src='.file_create_url($item['uri']).$url.'">';	

  return $output;
}
